﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

using FilePropertyEditor.PropertyLoader;
using mp3propertyeditor.FilePropertyEditor.LogWriter;

namespace mp3propertyeditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<string> audioFiles = new List<string>();

        public MainWindow()
        {
            InitializeComponent();
            Console.SetOut(new TextBoxLogWriter(textBox_Log));
            
        }

        private void ButtonClick_SaveChanges(object sender, RoutedEventArgs e)
        {
            List<string> files = listBox_EditFiles.Items.OfType<string>().ToList();
            for (int i = 0; i < files.Count; ++i)
            {
                IPropertyEditor propertyEditor = new PropertyEditor(files[i]);
                var artist = textBox_Artist.Text;
                var contributingArtist = textBox_ContributingArtist.Text;
                var albumName = textBox_Album.Text;
                var albumArtPath = textBox_AlbumArt.Text;
                var genre = textBox_Genre.Text;
                var year = textBox_Year.Text;
                var trackNumber = i + 1;
                
                if(!propertyEditor.EditTrackNumber(trackNumber))
                {
                    ShowErrorMessage();
                    continue;
                }

                if (!string.IsNullOrWhiteSpace(artist))
                {
                    if (!propertyEditor.EditArtist(new[] { artist }))
                    {
                        ShowErrorMessage();
                        continue;
                    }
                }

                if (!string.IsNullOrWhiteSpace(contributingArtist))
                {
                    if (!propertyEditor.EditContributingArtist(new[] { contributingArtist }))
                    {
                        ShowErrorMessage();
                        continue;
                    }
                }

                if (!string.IsNullOrWhiteSpace(albumName))
                {
                    if (!propertyEditor.EditAlbum(albumName))
                    {
                        ShowErrorMessage();
                        continue;
                    }
                }

                if (!string.IsNullOrWhiteSpace(genre))
                {
                    if (!propertyEditor.EditGenre(new[] { genre }))
                    {
                        ShowErrorMessage();
                        continue;
                    }
                }

                if (!string.IsNullOrWhiteSpace(year))
                {
                    if (!propertyEditor.EditYear(uint.Parse(year)))
                    {
                        ShowErrorMessage();
                        continue;
                    }
                }

                if (!string.IsNullOrWhiteSpace(albumArtPath))
                {
                    if (!propertyEditor.EditCoverArt(albumArtPath))
                    {
                        ShowErrorMessage();
                        continue;
                    }
                }

                if (!propertyEditor.SaveProperties())
                {
                    ShowErrorMessage();
                    continue;
                }
            }

            ShowSuccessMessage();
            return;
        }

        private void ShowErrorMessage()
        {
            Console.WriteLine("Failed to edit properties.");
            MessageBox.Show(
                "Failed to edit properties.",
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error,
                MessageBoxResult.Yes
            );
        }

        private void ShowSuccessMessage()
        {
            Console.WriteLine("Properties edited successfully!");
            MessageBox.Show(
                "Properties edited successfully!",
                "Success",
                MessageBoxButton.OK,
                MessageBoxImage.Information,
                MessageBoxResult.Yes
            );
        }

        private void ButtonClick_SelectAlbumArt(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = ""; // Default file name
            dlg.DefaultExt = ".png"; // Default file extension
            dlg.Filter = "Image files (*.png, *.jpg)|*.png;*.jpg"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                textBox_AlbumArt.Text = dlg.FileName;
            }
        }

        private void ButtonClick_SelectAudioFiles(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = ""; // Default file name
            dlg.DefaultExt = ".mp3"; // Default file extension
            dlg.Filter = "Audio files (*.mp3, *.mp4)|*.mp3;*.mp4"; // Filter files by extension
            dlg.Multiselect = true;

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                foreach(String file in dlg.FileNames)
                {
                    Console.WriteLine("[+] Adding file to list: " + file);
                    if (!audioFiles.Contains(file))
                    {
                        audioFiles.Add(file);
                        listBox_EditFiles.Items.Add(file);
                    }
                }
            }
        }

        private void ButtonClick_RemoveAudioFiles(object sender, RoutedEventArgs e)
        {
            List<string> selectedItems = listBox_EditFiles.SelectedItems.OfType<string>().ToList();
            foreach (string file in selectedItems)
            {
                audioFiles.Remove(file);
                listBox_EditFiles.Items.Remove(file);
                Console.WriteLine("[+] Removing file from list: " + file);
            }
        }
    }
}
