﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Controls;

namespace mp3propertyeditor.FilePropertyEditor.LogWriter
{
    class TextBoxLogWriter : TextWriter
    {
        private TextBox textBox;
        public TextBoxLogWriter(TextBox textBox)
        {
            this.textBox = textBox;
        }

        public override void WriteLine(string log)
        {
            textBox.Text += log + '\n';
        }

        public override Encoding Encoding
        {
            get { return Encoding.ASCII; }
        }
    }
}
