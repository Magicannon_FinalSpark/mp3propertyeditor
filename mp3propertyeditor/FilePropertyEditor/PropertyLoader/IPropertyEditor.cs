﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FilePropertyEditor.PropertyLoader
{
    interface IPropertyEditor
    {
        bool EditArtist(string [] artists);
        bool EditContributingArtist(string[] artists);
        bool EditAlbum(string album);
        bool EditTrackNumber(int trackNumber);
        bool EditCoverArt(string coverArtPath);
        bool EditYear(uint year);
        bool EditGenre(string[] genre);
        bool SaveProperties();
    }
}
