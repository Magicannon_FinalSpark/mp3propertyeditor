﻿using System;
using System.Collections.Generic;
using System.Text;
using TagLib;
using System.IO;


namespace FilePropertyEditor.PropertyLoader
{
    class PropertyEditor : IPropertyEditor
    {
        private TagLib.File file;
        public PropertyEditor(string filename)
        {
            file = TagLib.File.Create(filename);
        }

        public bool EditArtist(string[] artists)
        {
            bool result = true;
            try
            {
                file.Tag.AlbumArtists = artists;
            }
            catch
            {
                result = false;
            }
            return result;
        }
        public bool EditContributingArtist(string[] artists)
        {
            bool result = true;
            try
            {
                file.Tag.Performers = artists;
            }
            catch
            {
                result = false;
            }
            return result;
        }
        public bool EditAlbum(string album)
        {
            bool result = true;
            try
            {
                file.Tag.Album = album;
            }
            catch
            {
                result = false;
            }
            return result;
        }
        public bool EditTrackNumber(int trackNumber)
        {
            bool result = true;
            try
            {
                file.Tag.Track = (uint)trackNumber;
            }
            catch
            {
                result = false;
            }
            return result;
        }
        public bool EditCoverArt(string coverArtPath)
        {
            bool result = true;
            try
            {
                if (System.IO.File.Exists(coverArtPath))
                {
                    using (FileStream fileContent = System.IO.File.OpenRead(coverArtPath))
                    {
                        file.Tag.Pictures = new TagLib.IPicture[] { new TagLib.Picture(coverArtPath) };
                    }
                }
            }
            catch
            {
                result = false;
            }
            return result;
        }
        public bool EditYear(uint year)
        {
            bool result = true;
            try
            {
                file.Tag.Year = year;

            }
            catch
            {
                result = false;
            }
            return result;
        }
        public bool EditGenre(string[] genre)
        {
            bool result = true;
            try
            {
                file.Tag.Genres = genre;
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public bool SaveProperties()
        {
            bool result = true;
            try
            {
                file.Save();
            }
            catch
            {
                result = false;
            }
            return result;
        }
    }
}
